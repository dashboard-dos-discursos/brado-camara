import time
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
def epoch(value):
    try:
        return int(time.mktime(value.timetuple())*1000)
    except AttributeError:
        return ''

@register.filter
@stringfilter
def capitalname(value):
    full_name = ''
    for name in value.split():
        full_name += name.capitalize() + " "
    return full_name.strip()

