# -*- coding: utf-8 -*-
from dashboard.models import Discurso
from django.db import transaction
import nltk

queryset = Discurso.objects.filter(orador__nome="PLÍNIO SALGADO")
whole_speech = ''
for discurso in queryset:
    whole_speech += discurso.sumario + '\n'
tokens = nltk.word_tokenize(whole_speech.encode("utf-8"))
text = nltk.Text(tokens)

